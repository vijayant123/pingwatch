var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){

  console.log('here');

  socket.emit('pong', {
    'a': 'ok'
  })

  socket.on('p',function (e) {
    console.log('had a ping');
    socket.emit('pong', {
      'a': 'ok',
      'b': 1000
    });
  });


});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
